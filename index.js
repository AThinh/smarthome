var express = require("express");
var app = express();
app.use(express.static("public"));
app.set("view engine", "ejs");
app.set("views", "./views");
app.listen(3000);

var multer = require('multer');
var bodyParser = require('body-parser');

app.use(bodyParser.json());

// Connect with mongodb
var mongoose = require('mongoose');

//Lets connect to our database using the DB server URL.
mongoose.connect('mongodb://localhost/iot');

var Data = require('./app/models/data');
var User = require('./app/models/user');
var Sensor = require('./app/models/sensor');
var Device = require('./app/models/device');
var Date_noti = require('./app/models/date_noti');
app.get("/", function (req, res) {
  res.render("trangchu");
});

app.get('/api/data', function (req, res) {
  if (req.query.top == null) {
    Data.getData(function (err, data) {
      if (err) {
        throw err;
      }
      res.json(data);
    });
  }
  else {
    Data.getTop(function (err, data) {
      if (err) {
        throw err
      }
      res.json(data);
    }, req.query.top);
  }

});
app.get('/upload', function (req, res) {
  if (req.query.lux == null || req.query.rain == null || req.query.date == null || req.query.lamp == null)
    res.send({ success: "no", message: "Upload fail" });
  else {
    res.send({ success: "yes", message: "Upload complete" });
    var bodySensor = {
      lux: req.query.lux,
      rain: req.query.rain,
      date: req.query.date
    };
    var bodyDevice = {
      lamp: req.query.lamp
    };
    var body = {
      lux: req.query.lux,
      rain: req.query.rain,
      lamp: req.query.lamp,
      date: req.query.date
    };
    Sensor.add(bodySensor);
    Device.add(bodyDevice);
    Data.addData(body);
  }
});
app.get('/upload/device', function (req, res) {
  if (req.query.lamp == null)
    res.send({ success: "no", message: "Upload fail" });
  else {
    res.send({ success: "yes", message: "Upload complete" });
    var body = {
      lamp: req.query.lamp
    }
    Device.add(body);
  }
});
app.get('/upload/sensor', function (req, res) {
  if (req.query.lux == null || req.query.rain == null || req.query.date == null)
    res.send({ success: "no", message: "Upload fail" });
  else {
    res.send({ success: "yes", message: "Upload complete" });
    var bodySensor = {
      lux: req.query.lux,
      rain: req.query.rain,
      date: req.query.date
    };
    Sensor.add(bodySensor);
  }
});
app.get('/upload/datenoti', function (req, res) {
  if (req.query.date == null && req.query.noti == null)
    res.send({ success: "no", message: "Upload fail" });
  else {
    res.send({ success: "yes", message: "Upload complete" });
    var bodySensor = {
      noti: req.query.noti,
      date: req.query.date
    };
    Date_noti.add(bodySensor);
  }
});
app.get('/api/device', function (req, res) {
  Device.getTop(function (err, data) {
    if (err) {
      throw err
    }
    res.json(data);
  }, 1);
});
app.get('/api/datenoti', function (req, res) {
  Date_noti.getTop(function (err, data) {
    if (err) {
      throw err
    }
    res.json(data);
  }, 1);
});
app.get('/api/sensor', function (req, res) {
  // Sensor.getTop(function (err, data) {
  //   if (err) {
  //     throw err
  //   }
  //   res.json(data);
  // }, '1');
  if (req.query.top == null) {
    Sensor.getSensor(function (err, data) {
      if (err) {
        throw err;
      }
      res.json(data);
    });
  }
  else {
    Sensor.getTop(function (err, data) {
      if (err) {
        throw err
      }
      res.json(data);
    }, parseInt(req.query.top));
  }
});

app.get('/graph', function (req, res) {
  res.render('graph');
})
app.get('/api/checkAut', function (req, res) {
  if (req.query.user != null && req.query.pass != null) {
    User.checkUser(req.query.user, req.query.pass, function (err, data) {
      if (err)
        throw err;
      if (data == null) {
        res.send({
          success: "no",
          message: "Invalid username and password"
        })
      }
      else {
        res.send({
          success: "yes",
          message: "Valid username and password",
          token: data.token
        })
      }
    });
  }
  else {
    res.send([
      {
        success: "no",
        message: "wrong parameters"
      }
    ]);
  }
})
app.get('/data', function (req, res) {
  res.render('dataTable');
})
app.get('/about', function (req, res) {
  res.render('about');
})
app.get('/files', function (req, res) {
  res.render('fileServer');
})
app.get
// User.addUser([
//   {
//     user: "thinh",
//     pass: "123",
//     token: "czx2c121erKdasDAIX343XZndbmZC"
//   }
// ]);

// '''
// File server
// '''
var fs = require("fs"),
  path = require("path");
var Storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, "./Files");
  },
  filename: function (req, file, callback) {
    callback(null, file.originalname);
  }
});
var upload = multer({ storage: Storage }).array("imgUploader", 3); //Field name and max count
app.post("/files", function (req, res) {
  upload(req, res, function (err) {
    if (err) {
      return res.send("Something went wrong!");
    }
    res.render("fileServer");
  });
});
var p = "./Files"

app.get('/api/files', function (req, res) {
  var data = [];

  fs.readdir(p, function (err, files) {
    if (err) {
      throw err;
    }

    files.map(function (file) {
      return path.join(p, file);
    }).filter(function (file) {
      return fs.statSync(file).isFile();
    }).forEach(function (file) {
      // console.log("%s (%s)", file, path.extname(file));
      data.push({ file: file.replace('Files/', '') });
    });
    res.send(data);
  });

})
var path = require('path');
var mime = require('mime');

app.get('/download', function (req, res) {
  if (req.query.file != null) {
    var file = __dirname + '/Files/' + req.query.file;

    var filename = path.basename(file);
    var mimetype = mime.lookup(file);

    res.setHeader('Content-disposition', 'attachment; filename=' + filename);
    res.setHeader('Content-type', mimetype);

    var filestream = fs.createReadStream(file);
    filestream.pipe(res);
  }
  else {
    res.send({ success: "no", message: "no such file in directory" });
  }
});