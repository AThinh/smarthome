import bluetooth
import datetime
import requests
import thread
import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
#HC-06 mac-address
bd_addr = "98:D3:32:20:F6:2D"
port = 1

keys =["lux", "rain", "date"]
sensorUrl = "http://localhost:3000/upload/sensor?"
lampUrl = "http://localhost:3000/upload/device?"
deviceUrl = "http://localhost:3000/api/device"

state=0

LED_PIN = 4
GPIO.setup(LED_PIN, GPIO.OUT, initial=GPIO.LOW)
#func to connect hc-06
def connect():
    while(True):    
        try:
            socket = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
            socket.connect((bd_addr, port))
            #print type(socket)
            break;
        except bluetooth.btcommon.BluetoothError as error:
            print "Could not connect: ", error, "; Retrying in 5s..."
            time.sleep(5)
    return socket;

socket = connect()

def getData():
    global state
    global socket
    data = ""
    values = []
    payload = []
    while 1:
        try:
            data += socket.recv(25)
            data_end=data.find("\n")
            if data_end != -1:
                recv = data[:data_end]
                now = datetime.datetime.now().replace(microsecond=0)
                print recv
                values= recv.split(",")
                values.append(now.isoformat())
                payload.append(dict(zip(keys, values)))
                r = requests.get(sensorUrl, params=payload[0])
                print r.text
                if int(r.status_code) == 200:
                    payload.pop(0)
                    print "Get ok"
                else:
                    pass
                data = ""
            else:
                pass
        except bluetooth.btcommon.BluetoothError as blt_e:
            print "Caught BluetoothError: ", blt_e
            socket=connect()
        except KeyboardInterrupt:
            break
        except requests.exceptions.RequestException as rq_e:
            print "Caught RequestError", rq_e
        except :
            print "Error"
            break
    socket.close()

#recevee signal control
def recvSignal():
    try:
        r = requests.get(deviceUrl)
        if int(r.status_code) == 200:
            value = int(r.text[r.text.find("lamp") + 7])
            return value
        else:
            return -1
    except requests.exceptions.RequestException as rq_e:
        print "Caught RequestError", rq_e
        return -1
def controlLamp():
    global state
    
    while 1:
        try:
            payload = {'lamp': str(state)}
            temp = -1
            temp = recvSignal()
            if (temp != state) & (temp != -1):
                GPIO.output(4, temp)
                state = temp
                print "Change",GPIO.input(4)
            else:
                pass
            time.sleep(1)
        except:
            r = requests.get(lampUrl, params=payload)
            print "Control error"
try:
    thread.start_new_thread(getData,())
    thread.start_new_thread(controlLamp,())
except:
   print "Error: unable to start thread"         

while 1:
    pass
