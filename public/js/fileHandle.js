
$(document).ready(function () {
  var options = {
    beforeSubmit: showRequest,  // pre-submit callback
    success: showResponse  // post-submit callback
  };

  // bind to the form's submit event
  $('#frmUploader').submit(function () {
    $(this).ajaxSubmit(options);
    // always return false to prevent standard browser submit and page navigation
    return false;
  });
});

// pre-submit callback
function showRequest(formData, jqForm, options) {
  alert('Uploading is starting.');
  return true;
}

// post-submit callback
function showResponse(responseText, statusText, xhr, $form) {
  alert('status: ' + statusText + '\n\nresponseText: \n' + responseText);
}
var Event = React.createClass({
  upload() {
    var xmlHttp = new XMLHttpRequest();
    var theUrl = "download?file=" + this.props.name;
    window.open(theUrl);
    // xmlHttp.open("GET", theUrl, false); // false for synchronous request
    // xmlHttp.send(null);
  },
  getInitialState() {
    var res = this.props.name.split(".");

    return ({
      name: res[0],
      type: res[1]
    })
  },
  render: function () {
    return (
      <tbody>

        <tr>

          <td> <a onClick={this.upload}>{this.state.name}</a></td>
          <td> {this.state.type} </td>
        </tr>
      </tbody>
    )
  }
});
var ShowFile = React.createClass({
  add() {
    this.setState({ num: this.state.num + 1 });

  },
  getInitialState() {
    this.getData();
    return ({
      mang: [],
      num: 0
    })
  },
  render: function () {

    return (
      <div className="container">
        <h2>All File in Cloud</h2>
        <table className="table table-bordered">
          <thead>
            <tr>
              <th>Name</th>
              <th>Type</th>
            </tr>
          </thead>
          {
            this.state.mang.map(function (data) {
              return (
                <Event name={data.file}></Event>
              );
            })
          }
        </table>
      </div>

    )
  },
  alertNoti: function () {
    alert(this.state.mang.file);
  },
  getData: function (e) {
    fetch('/api/files').then(function (data) {
      return data.json();
    }).then(json => {
      this.setState({
        mang: json
      });
    });
  }
});
ReactDOM.render(
  <ShowFile> </ShowFile>
  , document.getElementById('file')
);
