
var updateInterval = 1000 * 10;
var updateRain = 1000 * 1;
function notiCheckBoxChecked() {
  return document.getElementById('NotiCheckBox').checked;
}
function sendNoti(apiKey, numbers, message, sender) {
  var url = "https://api.txtlocal.com/send/?apikey=" + apiKey + "&numbers=" + numbers + "&message=" + message + "&sender=" + sender;
  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open("GET", url, false); // false for synchronous request
  xmlHttp.send(null);
};
function getDateNoti() {
  return fetch('/api/datenoti')
    .then(res => res.json()
    ).then(out => {
      // Notification SMS to user
      if (out !== 'undefined' && out.length != 0) {
        var preDate = new Date(out[0].date);
        // var curDate = new Date();
        // var time_rr = 15 * 1000; //3h = 1 * 60 *60 *1000
        // if (((curDate - preDate) - time_rr) > 0) {
        //   // Update timer
        //   // var xmlHttp = new XMLHttpRequest();
        //   // xmlHttp.open("GET", "/upload/datenoti?date=" + curDate.toISOString(), false);
        //   // xmlHttp.send(null);
        //   //Send SMS
        //   // alert("SMS is send")
        // }
        $("#lastNoti").text(preDate);
      }
      else {
        $("#lastNoti").text("not yet");
      }
    });
};

var getData = function () {
  return fetch('/api/sensor?top=10')
    .then(res => res.json()
    ).then(out => {
      var temp = [];
      var i;
      for (i = 0; i < 10; i++) {
        var j = 9 - i;
        temp.push({ x: new Date(out[j].date), y: parseInt(out[j].lux) });
      }
      var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        title: {
          text: "Light"
        },
        axisX: {
          valueFormatString: "DD/MM | HH:mm:ss"
        },
        axisY: {
          title: "lux",
          includeZero: false,
          scaleBreaks: {
            autoCalculate: false
          }
        },
        data: [{
          type: "line",
          xValueFormatString: "YYYY-MM-DDTHH:mm:ss",
          color: "#F08080",
          dataPoints: temp
        }]
      });
      chart.render();
      var updateTimer = new Date();
      // Update weather status
      document.getElementById("timeUpdated").innerHTML = "Updated at " + updateTimer.getHours() + "h" + updateTimer.getMinutes() + "p" + updateTimer.getSeconds() + "s " + updateTimer.getDate() + "/" + updateTimer.getMonth();
      // if (out[0].rain == "0") {
      //   //alert(out[0].lamp);
      //   document.getElementById("weather").src = "img/weather_0.png";
      // }
      // else {
      //   //alert(out[0].lamp);
      //   document.getElementById("weather").src = "img/weather_1.png";
      // }
      // Notification SMS to user


    });
}
var getDataRain = function () {
  return fetch('/api/sensor?top=1')
    .then(res => res.json()
    ).then(out => {
      // var temp = [];
      // var i;
      // for (i = 0; i < 10; i++) {
      //   var j = 9 - i;
      //   temp.push({ x: new Date(out[j].date), y: parseInt(out[j].lux) });
      // }
      // var chart = new CanvasJS.Chart("chartContainer", {
      //   animationEnabled: true,
      //   title: {
      //     text: "Light"
      //   },
      //   axisX: {
      //     valueFormatString: "DD/MM | HH:mm:ss"
      //   },
      //   axisY: {
      //     title: "lux",
      //     includeZero: false,
      //     scaleBreaks: {
      //       autoCalculate: false
      //     }
      //   },
      //   data: [{
      //     type: "line",
      //     xValueFormatString: "YYYY-MM-DDTHH:mm:ss",
      //     color: "#F08080",
      //     dataPoints: temp
      //   }]
      // });
      // chart.render();
      // var updateTimer = new Date();
      // Update weather status
      // document.getElementById("timeUpdated").innerHTML = "Updated at " + updateTimer.getHours() + "h" + updateTimer.getMinutes() + "p" + updateTimer.getSeconds() + "s " + updateTimer.getDate() + "/" + updateTimer.getMonth();
      if (out[0].rain == "0") {
        //alert(out[0].lamp);
        document.getElementById("weather").src = "img/weather_0.png";
      }
      else {
        //alert(out[0].lamp);
        document.getElementById("weather").src = "img/weather_1.png";
      }
      // Notification SMS to user


    });
}


getData();
setInterval(function () { getData() }, updateInterval);
getDataRain();
setInterval(function () { getDataRain() }, updateRain);
