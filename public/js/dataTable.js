// var mongoose = require('mongoose');

// //Lets connect to our database using the DB server URL.
// mongoose.connect('mongodb://localhost/iot');

// var Data = require('./app/models/data');
// var a;
// Data.getData(function (err, data) {
//   if (err) {
//     throw err;
//   }
//   a = res.json(data);
// });
var a = 'thinh';
var list;
var Note = React.createClass({
  render: function () {
    return <div className="div-note">

      {this.props.children}

    </div>
  }
})
var List = React.createClass({
  getInitialState() {
    list = this;
    return { mang: ["thinh", "dep", "trai"] }
  },
  render: function () {
    return (
      <div className="div-note">
        {
          this.state.mang.map(function (note, index) {
            return <Note key={index}> {note} </Note>
          })
        }
      </div>
    );
  }
});
var Row = React.createClass({
  getInitialState() {
    return ({
      mang: []
    })
  },

  render: function () {

    return (
      <tbody>
        {
          this.state.mang.map((mang) =>
            <tr>
              <td> {mang.lux}</td>
              <td> {mang.rain} </td>
              <td> {mang.lamp} </td>
              <td> {mang.date} </td>
            </tr>
          )
        }
      </tbody>
    )
  },
  getData: function (e) {
    fetch('/api/data?top=5').then(function (data) {
      return data.json();
    }).then(json => {
      this.setState({
        mang: json
      });
    });
  },
  componentDidMount() {
    setInterval(this.getData, 1000);
  }
});
var Table = React.createClass({
  render: function () {
    return (
      <div className="container">
        <h2>Data Table</h2>
        <p>Top 5 lastest data:</p>
        <table className="table table-bordered">
          <thead>
            <tr>
              <th>Lux</th>
              <th>Rain</th>
              <th>Lamp Status</th>
              <th>Update time</th>
            </tr>
          </thead>
          <Row></Row>
        </table>
      </div>
    )
  }
})
ReactDOM.render(
  <Table> </Table>
  , document.getElementById('root')
);

