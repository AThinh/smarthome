const mongoose = require('mongoose');
const dataScheme = mongoose.Schema(
  {
    lux: String,
    rain: String,
    lamp: String,
    date: String
  }
)

const Data = module.exports = mongoose.model('data', dataScheme);

module.exports.getData = (callback, limit) => {
  Data.find(callback).limit(limit);
}
module.exports.getTop = (callback, top) => {
  Data.find(callback).limit(top).sort({ $natural: -1 });
}
// Add Genre
module.exports.addData = (data) => {
  Data.create(data);
}

// Update Genre
module.exports.updateData = (id, genre, options, callback) => {
  var query = { _id: id };
  var update = {
    name: genre.name
  }
  Data.findOneAndUpdate(query, update, options, callback);
}


// Delete Genre
module.exports.removeData = (id) => {
  var query = { _id: id };
  Data.remove(query);
}