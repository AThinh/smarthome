const mongoose = require('mongoose');
const Date_notiScheme = mongoose.Schema(
  {
    noti: String,
    date: String
  }
)

const Date_noti = module.exports = mongoose.model('Date_noti', Date_notiScheme);

module.exports.getDate_noti = (callback, limit) => {
  Date_noti.find(callback).limit(limit);
}
module.exports.getTop = (callback, top) => {
  Date_noti.find(callback).limit(top).sort({ $natural: -1 });
}
// Add Genre
module.exports.add = (data) => {
  Date_noti.create(data);
}

// Delete Genre
module.exports.removeDate_noti = (id) => {
  var query = { _id: id };
  Date_noti.remove(query);
}