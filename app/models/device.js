const mongoose = require('mongoose');
const DeviceScheme = mongoose.Schema(
  {
    lamp: String
  }
)

const Device = module.exports = mongoose.model('Device', DeviceScheme);

module.exports.getDevice = (callback, limit) => {
  Device.find(callback).limit(limit);
}
module.exports.getTop = (callback, top) => {
  Device.find(callback).limit(1).sort({ $natural: -1 });
}
// Add Genre
module.exports.add = (data) => {
  Device.create(data);
}

// Delete Genre
module.exports.removeDevice = (id) => {
  var query = { _id: id };
  Device.remove(query);
}