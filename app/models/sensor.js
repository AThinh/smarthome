const mongoose = require('mongoose');
const SensorScheme = mongoose.Schema(
  {
    lux: String,
    rain: String,
    date: String
  }
)

const Sensor = module.exports = mongoose.model('Sensor', SensorScheme);

module.exports.getSensor = (callback, limit) => {
  Sensor.find(callback).limit(limit);
}
module.exports.getTop = (callback, top) => {
  Sensor.find(callback).limit(top).sort({ $natural: -1 });
}
// Add Genre
module.exports.add = (data) => {
  Sensor.create(data);
}
// Delete Genre
module.exports.removeSensor = (id) => {
  var query = { _id: id };
  Sensor.remove(query);
}