const mongoose = require('mongoose');
const userScheme = mongoose.Schema(
  {
    user: String,
    pass: String,
    token: String
  }
)

const User = module.exports = mongoose.model('user', userScheme);

module.exports.getData = (callback, limit) => {
  User.find(callback).limit(limit);
}
module.exports.getTop = (callback, top) => {
  User.find(callback).limit(top).sort({ $natural: -1 });
}

// Check user
module.exports.checkUser = function (username, password, callback) {
  var body = { user: username, pass: password };
  User.findOne(body, callback);
}
// Update Genre
module.exports.updateData = (id, genre, options, callback) => {
  var query = { _id: id };
  var update = {
    name: genre.name
  }
  User.findOneAndUpdate(query, update, options, callback);
}


module.exports.addUser = (data) => {
  User.create(data);
}