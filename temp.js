var fs = require("fs"),
  path = require("path");

var p = "/home/antchil/"
fs.readdir(p, function (err, files) {
  if (err) {
    throw err;
  }

  files.map(function (file) {
    return path.join(p, file);
  }).filter(function (file) {
    return fs.statSync(file).isFile();
  }).forEach(function (file) {
    // console.log("%s (%s)", file, path.extname(file));
    var data = { result: file.replace(p, '') };
    console.log(data);
  });
});